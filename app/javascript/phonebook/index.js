const base_url = window.location
const phonebook_api_url = `${base_url}phonebooks`

const methods = {
    btnCreateClick() {
        const modal = new bootstrap.Modal(document.getElementById('modal-form-create'), {})
        const form = document.getElementById('form-modal-create')
        form['id'].value = ""
        form['first_name'].value = ""
        form['last_name'].value = ""
        form['tag'].value = ""
        form['phone'].value = ""
        form['email'].value = ""
        form['instagram'].value = ""
        form['twitter'].value = ""
        form['linkedin'].value = ""
        modal.show()
        const btnSubmit = document.getElementsByClassName('btn-submit-create')
        Array.from(btnSubmit).forEach(btn => {
            btn.addEventListener('click', methods.sendNewData)
        })
    },
    btnEditClick() {
        const id = this.dataset['id']
        const modal = new bootstrap.Modal(document.getElementById('modal-form-edit'), {})
        $.ajax({
            type: 'GET',
            url: `${phonebook_api_url}/${id}`,
            contentType: "application/json",
            dataType: "json",
            success: (data) => {
                methods.injectDataToForm(data)
                const btnSubmit = document.getElementsByClassName('btn-submit-edit')
                Array.from(btnSubmit).forEach(btn => {
                    btn.addEventListener('click', methods.sendEditData)
                })
            }
        })
        modal.show()
    },
    btnShowClick() {
        const id = this.dataset['id']
        const modal = new bootstrap.Modal(document.getElementById('modal-detail'), {})
        $.ajax({
            type: 'GET',
            contentType: "application/json",
            dataType: "json",
            url: `${phonebook_api_url}/${id}`,
            success: (data) => {
                methods.injectDataToModal(data)
                modal.show()
            }
        })
    },
    btnDeleteClick() {
        if (confirm("Apakah Anda Yakin Menghapus Data Ini?") == true) {
            const id = this.dataset['id']
            $.ajax({
                type: 'DELETE',
                contentType: "application/json",
                dataType: "json",
                beforeSend: function(xhr) {xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))},
                url: `${phonebook_api_url}/${id}`,
                success: (data) => {
                    methods.renderIndexData()
                    let toastEl = document.getElementById('toast-success')
                    let toastBody = document.getElementById('toast-alert-success')
                    toastBody.innerHTML = data.response
                    const toast = new bootstrap.Toast(toastEl,{})
                    toast.show()
                }
            })
        } else {
            return
        }
    },
    injectDataToModal(data) {
        const field = document.getElementById('detail-field')
        field.innerHTML = ''
        const name_label = document.createElement('dt')
        name_label.innerHTML = "Nama Lengkap"
        field.appendChild(name_label)
        const name_field = document.createElement('dd')
        name_field.innerHTML = `${data.first_name} ${data.last_name}`
        field.appendChild(name_field)
        const tag_label = document.createElement('dt')
        tag_label.innerHTML = "Tag"
        field.appendChild(tag_label)
        const tag_field = document.createElement('dd')
        tag_field.innerHTML = `${data.tag}`
        field.appendChild(tag_field)
        const email_label = document.createElement('dt')
        email_label.innerHTML = "Email"
        field.appendChild(email_label)
        const email_field = document.createElement('dd')
        email_field.innerHTML = `${data.email}`
        field.appendChild(email_field)
        const phone_label = document.createElement('dt')
        phone_label.innerHTML = "Telepon"
        field.appendChild(phone_label)
        const phone_field = document.createElement('dd')
        phone_field.innerHTML = `${data.phone}`
        field.appendChild(phone_field)
        const instagram_label = document.createElement('dt')
        instagram_label.innerHTML = "Instagram"
        field.appendChild(instagram_label)
        const instagram_field = document.createElement('dd')
        instagram_field.innerHTML = `<a href="https://www.instagram.com/${data.instagram}">${data.instagram}</a>`
        field.appendChild(instagram_field)
        const twitter_label = document.createElement('dt')
        twitter_label.innerHTML = "Twitter"
        field.appendChild(twitter_label)
        const twitter_field = document.createElement('dd')
        twitter_field.innerHTML = `<a href="https://www.twitter.com/${data.twitter}">${data.twitter}</a>`
        field.appendChild(twitter_field)
        const linkedin_label = document.createElement('dt')
        linkedin_label.innerHTML = "Linkedin"
        field.appendChild(linkedin_label)
        const linkedin_field = document.createElement('dd')
        linkedin_field.innerHTML = `<a href="https://www.linkedin.com/in/${data.linkedin}">${data.linkedin}</a>`
        field.appendChild(linkedin_field)
    },
    injectDataToForm(data) {
        const form = document.getElementById('form-modal-edit')
        form['id'].value = data.id
        form['first_name'].value = data.first_name
        form['last_name'].value = data.last_name
        form['tag'].value = data.tag
        form['phone'].value = data.phone
        form['email'].value = data.email
        form['instagram'].value = data.instagram
        form['twitter'].value = data.twitter
        form['linkedin'].value = data.linkedin
    },
    sendNewData() {
        const form = document.getElementById('form-modal-create')
        const errorCardField = document.getElementById('error-card-field')
        let errors = methods.validateInput(form)

        if(errors.length !== 0)
        {
            errorCardField.innerHTML = ""
            const errorList = document.createElement('ul')
            errors.forEach(error => {
                const errorL = document.createElement('li')
                errorL.innerHTML = error
                errorList.appendChild(errorL)
            })
            errorCardField.appendChild(errorList)
            errorCardField.parentElement.hidden = false
        }else
        {
            const errorCardField = document.getElementById('error-card-field')
            errorCardField.parentElement.hidden = true
            $.ajax({
                type: 'POST',
                beforeSend: function(xhr) {xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))},
                contentType: "application/json",
                dataType: "json",
                url: `${phonebook_api_url}`,
                data: JSON.stringify({
                    'phonebook': {
                        'first_name': form['first_name'].value,
                        'last_name': form['last_name'].value,
                        'tag': form['tag'].value,
                        'email': form['email'].value,
                        'phone': form['phone'].value,
                        'instagram': form['instagram'].value,
                        'twitter': form['twitter'].value,
                        'linkedin': form['linkedin'].value,
                    }
                }),
                success: (data) => {
                    methods.renderIndexData()
                    let toastEl = document.getElementById('toast-success')
                    let toastBody = document.getElementById('toast-alert-success')
                    toastBody.innerHTML = data.response
                    let createModalEl = document.getElementById('modal-form-create')
                    let modal = bootstrap.Modal.getInstance(createModalEl)
                    modal.hide()
                    const toast = new bootstrap.Toast(toastEl,{})
                    toast.show()            
                },
                error: (data) => {
                    errorCardField.innerHTML = ""
                    const errorList = document.createElement('ul')
                    Array.from(data.response).forEach(error => {
                        const errorL = document.createElement('li')
                        errorL.innerHTML = error
                        errorList.appendChild(errorL)
                    })
                    errorCardField.appendChild(errorList)
                    errorCardField.parentElement.hidden = false
                    form.show()
                }
            })
        }
    },
    sendEditData() {
        const form = document.getElementById('form-modal-edit')
        const errorCardField = document.getElementById('error-card-field')
        let errors = methods.validateInput(form)

        if(errors.length !== 0)
        {
            errorCardField.innerHTML = ""
            const errorList = document.createElement('ul')
            errors.forEach(error => {
                const errorL = document.createElement('li')
                errorL.innerHTML = error
                errorList.appendChild(errorL)
            })
            errorCardField.appendChild(errorList)
            errorCardField.parentElement.hidden = false
        }else
        {
            const id = form['id'].value
            const errorCardField = document.getElementById('error-card-field')
            errorCardField.parentElement.hidden = true
            $.ajax({
                type: 'PUT',
                beforeSend: function(xhr) {xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))},
                dataType:"json",
                contentType:"application/json",
                url: `${phonebook_api_url}/${id}`,
                data: JSON.stringify({
                    'phonebook': {
                        'first_name': form['first_name'].value,
                        'last_name': form['last_name'].value,
                        'tag': form['tag'].value,
                        'email': form['email'].value,
                        'phone': form['phone'].value,
                        'instagram': form['instagram'].value,
                        'twitter': form['twitter'].value,
                        'linkedin': form['linkedin'].value,
                    }
                }),
                success: (data) => {
                    methods.renderIndexData()
                    let toastEl = document.getElementById('toast-success')
                    let toastBody = document.getElementById('toast-alert-success')
                    toastBody.innerHTML = data.response
                    let editModalEl = document.getElementById('modal-form-edit')
                    let modal = bootstrap.Modal.getInstance(editModalEl)
                    modal.hide()
                    const toast = new bootstrap.Toast(toastEl,{})
                    toast.show()
                },
                error: (data) => {
                    errorCardField.innerHTML = ""
                    const errorList = document.createElement('ul')
                    Array.from(data.response).forEach(error => {
                        const errorL = document.createElement('li')
                        errorL.innerHTML = error
                        errorList.appendChild(errorL)
                    })
                    errorCardField.appendChild(errorList)
                    errorCardField.parentElement.hidden = false
                    form.show()
                }
            })
        }
    },
    validateInput(form) {
        const errors = []
        if (form['first_name'].value=='') {
            form['first_name'].classList.add('is-invalid')
            errors.push('Nama Depan Harus Diisi')
        } else {
            form['first_name'].classList.remove('is-invalid')
        }
        if (form['last_name'].value=='') {
            form['last_name'].classList.add('is-invalid')
            errors.push('Nama Akhir Harus Diisi')
        } else {
            form['last_name'].classList.remove('is-invalid')
        }
        if (form['phone'].value=='') {
            form['phone'].classList.add('is-invalid')
            errors.push('Telepon Harus Diisi')
        } else {
            form['phone'].classList.remove('is-invalid')
        }
        if(isNaN(form['phone'].value)){
            form['phone'].classList.add('is-invalid')
            errors.push('Telepon Harus Diisi Dengan Nomor')
        } else {
            form['phone'].classList.remove('is-invalid')
        }
        if (!form['email'].value.includes('@')) {
            form['email'].classList.add('is-invalid')
            errors.push('Email Harus Diisi Alamat Email yang Valid')
        } else {
            form['email'].classList.remove('is-invalid')
        }
        return errors
    },
    renderIndexData() {
        $.ajax({
            type:"GET",
            contentType: "application/json",
            dataType:"json",
            url: `${base_url}/api/phonebooks/indexData`,
            success: (data) => {
                const tableField = document.getElementById('phonebook-table')
                tableField.innerHTML = ""
                if(data.contacts.length > 0){
                    const contactCount = document.getElementById('contact-count')
                    contactCount.innerHTML = data.count_contacts
                    data.contacts.forEach((contact) => {
                        const tableRow = document.createElement('tr')
                        const tableDataName = document.createElement('td')
                        tableDataName.innerHTML = `${contact.first_name} ${contact.last_name}`
                        tableRow.appendChild(tableDataName)
                        const tableDataAction = document.createElement('td')
                        const groupButtonTableDataAction = document.createElement('div')
                        groupButtonTableDataAction.classList.add("btn-group")
                        const buttonShow = document.createElement('button')
                        buttonShow.innerHTML = "Tampil"
                        buttonShow.classList.add('btn', 'btn-primary', 'btn-sm', 'btn-show')
                        buttonShow.setAttribute('data-id', contact.id)
                        const buttonEdit = document.createElement('button')
                        buttonEdit.innerHTML = "Ubah"
                        buttonEdit.classList.add('btn', 'btn-warning', 'btn-sm', 'btn-edit')
                        buttonEdit.setAttribute('data-id', contact.id)
                        const buttonDelete = document.createElement('button')
                        buttonDelete.innerHTML = "Hapus"
                        buttonDelete.classList.add('btn', 'btn-danger', 'btn-sm', 'btn-delete')
                        buttonDelete.setAttribute('data-id', contact.id)
                        groupButtonTableDataAction.appendChild(buttonShow)
                        groupButtonTableDataAction.appendChild(buttonEdit)
                        groupButtonTableDataAction.appendChild(buttonDelete)
                        tableDataAction.appendChild(groupButtonTableDataAction)
                        tableRow.appendChild(tableDataAction)
                        tableField.appendChild(tableRow)
                        buttonShow.addEventListener('click', methods.btnShowClick)
                        buttonEdit.addEventListener('click', methods.btnEditClick)
                        buttonDelete.addEventListener('click', methods.btnDeleteClick)
                    })
                }else{
                    const divInfo = document.createElement('h3')
                    divInfo.innerHTML = "Tidak Ada Data"
                    tableField.appendChild(divInfo)
                }
            }
        })
    }
}

document.addEventListener('DOMContentLoaded', (event) => {
    const btnCreate = document.getElementsByClassName('btn-create')
    // const btnEdits = document.getElementsByClassName('btn-edit')
    // const btnShows = document.getElementsByClassName('btn-show')
    // const btnDeletes = document.getElementsByClassName('btn-delete')
    Array.from(btnCreate).forEach(btn => {
        btn.addEventListener('click', methods.btnCreateClick)
    })
    // Array.from(btnEdits).forEach(btn => {
    //     btn.addEventListener('click', methods.btnEditClick)
    // })
    // Array.from(btnShows).forEach(btn => {
    //     btn.addEventListener('click', methods.btnShowClick)
    // })
    // Array.from(btnDeletes).forEach(btn => {
    //     btn.addEventListener('click', methods.btnDeleteClick)
    // })
    methods.renderIndexData()
})