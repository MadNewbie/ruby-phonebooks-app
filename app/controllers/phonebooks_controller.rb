class PhonebooksController < ApplicationController
    def index
        @count_contacts = Phonebook.count
        @phonebooks = Phonebook.all
    end

    def show
        @phonebook = Phonebook.find(params[:id])

        respond_to do |format|
            format.html
            format.json {render json: @phonebook, status: :ok}
        end
    end

    def new
        @phonebook = Phonebook.new()
    end

    def create
        @phonebook = Phonebook.new(phonebook_params)

        if @phonebook.save
            respond_to do |format|
                format.html {redirect_to @phonebook}
                format.json {render json: {response: "Data Berhasil Tersimpan"}, status: :ok}
            end
        else
            respond_to do |format|
                format.html {render :new, status: :unprocessable_entity}
                format.json {render json: {response: "Data Gagal Tersimpan"}, status: :unprocessable_entity}
            end
        end
    end

    def edit
        @phonebook = Phonebook.find(params[:id])
    end

    def update
        @phonebook = Phonebook.find(params[:id])

        if @phonebook.update(phonebook_params)
            respond_to do |format|
                format.html {redirect_to @phonebook}
                format.json {render json: {response: "Data Berhasil Terubah"}, status: :ok}
            end
        else
            respond_to do |format|
                format.html {render :edit, status: :unprocessable_entity}
                format.json {render json: {response: "Data Berhasil Terubah"}, status: :ok}
            end
        end
    end

    def destroy
        @phonebook = Phonebook.find(params[:id])
        @phonebook.destroy

        respond_to do |format|
            format.html {redirect_to phonebooks_path, status: :see_other}
            format.json {render json: {response: "Data Berhasil Dihapus"}, status: :ok}
        end
    end

    def api_indexData
        @count_contacts = Phonebook.count
        @phonebook = Phonebook.select("first_name, last_name, id")
        render json: {contacts: @phonebook, count_contacts: @count_contacts}, status: :ok
    end

    # def api_show
    #     @phonebook = Phonebook.find(params[:id])
    #     render json: @phonebook, status: :ok
    # end

    # def api_update
    #     @phonebook = Phonebook.find(params[:id])

    #     if @phonebook.update(phonebook_params)
    #         render json: {response: "Data Berhasil Terubah"}, status: :ok
    #     else
    #         render json: {response: "Data Gagal Tersimpan"}, status: :unprocessable_entity
    #     end
    # end

    # def api_create
    #     @phonebook = Phonebook.new(phonebook_params)

    #     if @phonebook.save
    #         render json: {response: "Data Berhasil Tersimpan"}, status: :ok
    #     else
    #         render json: {response: "Data Gagal Tersimpan"}, status: :unprocessable_entity
    #     end
    # end

    # def api_destroy
    #     @phonebook = Phonebook.find(params[:id])
    #     @phonebook.destroy

    #     render json: {response: "Data Berhasil Dihapus"}, status: :ok
    # end

    private
        def phonebook_params
            params.require(:phonebook).permit(:first_name, :last_name, :email, :phone, :instagram, :twitter, :linkedin, :tag)
        end
end
