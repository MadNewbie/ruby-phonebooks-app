class CreatePhonebooks < ActiveRecord::Migration[7.0]
  def change
    create_table :phonebooks do |t|
      t.string :first_name
      t.string :last_name
      t.string :email
      t.string :phone
      t.string :instagram
      t.string :twitter
      t.string :linkedin
      t.string :tag

      t.timestamps
    end
  end
end
