Rails.application.routes.draw do
  root "phonebooks#index"
  resources :phonebooks

  get "api/phonebooks/indexData", to: "phonebooks#api_indexData"
  get "api/phonebooks/counter", to: "phonebooks#api_counter", as: "api_contact_counter"
  # get "api/phonebooks/:id", to: "phonebooks#api_show"
  # put "api/phonebooks/:id", to: "phonebooks#api_update"
  # post "api/phonebooks/", to: "phonebooks#api_create"
  # delete "api/phonebooks/:id", to: "phonebooks#api_destroy"
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  # root "articles#index"
end
